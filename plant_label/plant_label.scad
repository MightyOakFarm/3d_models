// Dimensions of the label itself. Depending on these and the font size, the positioning of the text might get messed up.
HEIGHT = 50;
WIDTH = 10;
THICKNESS = 1;

// Dimensions of the decorative border along the edge. Set these to 0 if you want to get rid of the border.
BORDER_THICKNESS = 0.25;
BORDER_WIDTH = 0.5;

// Use any system font here to change up the font. SUB_TEXT_FONT does not need to be the same as MAIN_FONT.
MAIN_FONT = "Outfit:style=Regular";
SUB_TEXT_FONT = MAIN_FONT;
// These will likely need to change based on your dimensions.
MAIN_FONT_SIZE = 2;
SUB_TEXT_FONT_SIZE = 1;
TEXT_HEIGHT = 0.25;

// Your label text
MAIN_TEXT = "GREEN ZEBRA";
SUB_TEXT = "TOMATO";

module plant_label() {
  module label_border() {
    difference() {
      cube([WIDTH, HEIGHT, BORDER_THICKNESS]);

      translate([BORDER_WIDTH, -BORDER_WIDTH, -0.1])
      cube([WIDTH - (2 * BORDER_WIDTH), HEIGHT, 3 * BORDER_THICKNESS]);
    }
  }

  module point_cutter() {
    translate([0, 0, -0.5])
    cylinder(r = WIDTH / 2, h = THICKNESS * 2, $fn = 3);

    mirror([1, 0, 0]) {
      translate([-WIDTH, 0, -0.5])
      cylinder(r = WIDTH / 2, h = THICKNESS * 2, $fn = 3);
    }
  }

  union() {
    difference() {
      union() {
        cube([WIDTH, HEIGHT, THICKNESS]);

        translate([0, 0, THICKNESS])
        label_border();
      }

      point_cutter();
    }

    color("blue")
    linear_extrude(THICKNESS + TEXT_HEIGHT) {
      // Tweak 2.5 if your text positioning gets messed up from your custom dimensions.
      translate([(WIDTH / 2.5) + 1, HEIGHT - 2, 0])
      rotate([0, 0, -90])
      text(MAIN_TEXT, font = MAIN_FONT, size = MAIN_FONT_SIZE);

      // Tweak 5 if your text positioning gets messed up from your custom dimensions.
      translate([(WIDTH / 5) + 1, HEIGHT - 2, 0])
      rotate([0, 0, -90])
      text(SUB_TEXT, font = MAIN_FONT, size = SUB_TEXT_FONT_SIZE);
    }
  }
}

plant_label();
