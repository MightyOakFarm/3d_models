# Plant Label
We use these to keep track of our seedlings. It seems sort of wasteful, but using wooden sticks usually ends in disaster from rot. Thin plastic labels blow away, fall off or fade. We grow a lot of the same varieties year after year so having some re-usable labels is handy.

## How to use
The .scad file has plenty of comments to walk you through the configuration of your soil blocker. You should be able to change the variables according to the comments and be on your way.

One thing that is difficult with this print is making the dimensions AND the font customizable and keeping it all lined up properly. There are some comments where you might need to tweak actual code to get the font to line up correctly.
