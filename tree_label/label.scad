LENGTH = 40;
WIDTH = 40;
THICKNESS = 1;
BORDER_RADIUS = 5;

BORDER_THICKNESS = 0.25;
BORDER_WIDTH = 2;

HOLE_DIAMETER = 2;

MAIN_TEXT = "HONEYCRISP";
MAIN_TEXT_SIZE = 3;
MAIN_FONT = "YoungSerif:style=Regular";

SUB_TEXT = "APPLE";
SUB_TEXT_SIZE = 2.5;
SUB_TEXT_FONT = MAIN_FONT;

module rounded_cube(dimensions, border_radius) {
  translate([ border_radius, border_radius, 0 ])
  linear_extrude(dimensions[2]) {
    minkowski() {
      square([dimensions[0] - 2 * border_radius, dimensions[1] - 2 * border_radius]);
      circle(border_radius);
    }
  }
}

module draw_label() {
  module draw_border() {
    difference() {
      rounded_cube([WIDTH, LENGTH, BORDER_THICKNESS], BORDER_RADIUS);
      translate([BORDER_WIDTH, BORDER_WIDTH, -BORDER_THICKNESS])
      rounded_cube([WIDTH - 2 * BORDER_WIDTH, LENGTH - 2 * BORDER_WIDTH, BORDER_THICKNESS + 4 * BORDER_THICKNESS], BORDER_RADIUS);
    }  
  }
  
  difference() {
    union() {
      rounded_cube([WIDTH, LENGTH, THICKNESS], BORDER_RADIUS);
  
      translate([0, 0, THICKNESS])
      draw_border();
     
      color("blue")
      linear_extrude(THICKNESS + BORDER_THICKNESS) {
        translate([WIDTH / 2, LENGTH / 1.8 ,0])
        text(MAIN_TEXT, font = MAIN_FONT, size = MAIN_TEXT_SIZE, halign = "center");
        
        translate([WIDTH / 2, LENGTH / 2.5, 0])
        text(SUB_TEXT, font = SUB_TEXT_FONT, size = SUB_TEXT_SIZE, halign = "center");
      } 
    }
    
    translate([WIDTH / 2, LENGTH - 3 * BORDER_WIDTH, THICKNESS])
    cylinder(h = THICKNESS * 4, d = HOLE_DIAMETER, center = true, $fn = 18);
  }
}

draw_label();

