# 3D Models

This repository contains a collection of OpenSCAD models files that can be 3D printed for use around a garden/small farm. Each folder contains a configurable OpenSCAD file that we encourage you to use. We welcome other users to contribute improvements to these designs.

All of these designs are printed using PLA. Despite what you might read online, PLA holds up quite well when used outdoors. PLA is biodegradable under industrial conditions and even on a hot humid day, PLA won't be close to breaking down.