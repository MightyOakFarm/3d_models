// These variables are the dimensions of the soil block itself.
BLOCK_WIDTH = 20;
BLOCK_LENGTH = 20;
BLOCK_HEIGHT = 20;

// This is the thickness of the walls that will fill with soil. Thinner is better, but this
// tool is put through a lot of pressure so tread lightly. 2mm seems to be a good balance.
THICKNESS = 2;

// This is the size of the grid of blocks. Wide X values and small Y values make the tool easier
// to hold, but smaller blocks would change this.
NUM_BLOCKS_X = 6;
NUM_BLOCKS_Y = 4;

// These are the dimensions of the indent left for the seed. If you're printing nested soil blockers
// you may want these to be the same as another print's BLOCK_WIDTH, BLOCK_LENGTH, and BLOCK_HEIGHT.
SEED_HOLE_WIDTH = 4;
SEED_HOLE_LENGTH = 4;
SEED_HOLE_HEIGHT = 4;

// Information about the bolts you'll be using to attach the top bracket to the sides.
// For BOLT_HEAD_DIAMETER, measure the actual thickness of your bolt and add 1 since printers
// aren't the most precise. No need to add anything to BOLT_HEAD_DIAMETER.
BOLT_DIAMETER = 7;
BOLT_HEAD_DIAMETER = 25;

// This value generally doesn't need to be changed. This is the plate that has all the block pressers
// attached to it. If you were printing a massive soil blocker, this might need to be thicker than 6mm.
PLATE_HEIGHT = 6;

// These are helper variables and should not be changed.
DEVICE_LENGTH = (NUM_BLOCKS_Y * BLOCK_LENGTH) + (NUM_BLOCKS_Y + 1) * THICKNESS;
DEVICE_WIDTH = (NUM_BLOCKS_X * BLOCK_WIDTH) + (NUM_BLOCKS_X + 1) * THICKNESS;

// This is a ratio that I found worked well for how wide the side supports need to be. You could change it or
// hard code it if you had a better idea for your dimensions.
SIDE_SUPPORT_WIDTH = DEVICE_LENGTH / 2.5;

// These are helper variables and should not be changed.
OUTER_BLOCK_WIDTH = BLOCK_WIDTH + (2 * THICKNESS);
OUTER_BLOCK_LENGTH = BLOCK_LENGTH + (2 * THICKNESS);
BOLT_HEAD_RADIUS = BOLT_HEAD_DIAMETER / 2;

// The bottom shell is the part of the tool that will fill with dirt.
module draw_bottom_shell() {
    module draw_outer_block() {
    difference() {
      cube([OUTER_BLOCK_WIDTH, OUTER_BLOCK_LENGTH, BLOCK_HEIGHT]);

      translate([THICKNESS, THICKNESS, -5])
      cube([BLOCK_WIDTH, BLOCK_LENGTH, BLOCK_HEIGHT + 10]);
    }
  }

  side_support_dimensions = [THICKNESS * 2, SIDE_SUPPORT_WIDTH, (2 * BLOCK_HEIGHT) + PLATE_HEIGHT + BOLT_HEAD_DIAMETER];

  difference() {
    union() {
      for (x = [0:NUM_BLOCKS_X-1])
        for (y = [0:NUM_BLOCKS_Y-1])
          translate([x * (BLOCK_WIDTH + THICKNESS), y * (BLOCK_LENGTH + THICKNESS), 0])
          draw_outer_block();

      translate([-THICKNESS, (DEVICE_LENGTH - SIDE_SUPPORT_WIDTH) / 2, 0])
      cube(side_support_dimensions);

      translate([DEVICE_WIDTH - THICKNESS , (DEVICE_LENGTH - SIDE_SUPPORT_WIDTH) / 2, 0])
      cube(side_support_dimensions);
    }

    translate([DEVICE_WIDTH / 2, DEVICE_LENGTH / 2, (2 * BLOCK_HEIGHT) + PLATE_HEIGHT + BOLT_HEAD_RADIUS])
    rotate([0, 90, 0])
    cylinder(h = DEVICE_WIDTH + 10, d = BOLT_DIAMETER, center = true, $fn = 18);
  }
}

// The "presser" are the blocks that will shove the dirt out of the bottom shell.
module draw_presser() {
  module draw_presser_block() {
    translate([0.5, 0.5, 0])
    cube([BLOCK_WIDTH - 1, BLOCK_LENGTH - 1, BLOCK_HEIGHT + 6]);

    translate([(BLOCK_WIDTH - SEED_HOLE_WIDTH) / 2, (BLOCK_LENGTH - SEED_HOLE_LENGTH) / 2, -SEED_HOLE_HEIGHT])
    cube([SEED_HOLE_WIDTH, SEED_HOLE_LENGTH, SEED_HOLE_HEIGHT]);
  }

  difference() {
    union() {
      tab_width = 15 - THICKNESS;

      translate([-tab_width, THICKNESS, BLOCK_HEIGHT])
      cube([DEVICE_WIDTH + tab_width * 2, DEVICE_LENGTH - (2 * THICKNESS), PLATE_HEIGHT]);

      translate([THICKNESS, THICKNESS, 0])
      for (x = [0:NUM_BLOCKS_X-1])
        for (y = [0:NUM_BLOCKS_Y-1])
          translate([x * (BLOCK_WIDTH + THICKNESS), y * (BLOCK_LENGTH + THICKNESS), 0])
          draw_presser_block();
    }

    side_support_hole_dimensions = [(2 * THICKNESS) + 1, SIDE_SUPPORT_WIDTH + 1, 4 * PLATE_HEIGHT];

    translate([DEVICE_WIDTH - THICKNESS - 0.5 , ((DEVICE_LENGTH - SIDE_SUPPORT_WIDTH) / 2) - 0.5, BLOCK_HEIGHT - PLATE_HEIGHT])
    cube(side_support_hole_dimensions);

    translate([-THICKNESS - 0.5, ((DEVICE_LENGTH - SIDE_SUPPORT_WIDTH) / 2) - 0.5, BLOCK_HEIGHT - PLATE_HEIGHT])
    cube(side_support_hole_dimensions);
  }
}

// This acts partially as a handle, but mainly to keep the side rails straight and limit the
// vertical travel of the presser.
module top_bracket() {
  difference() {
    union() {
      translate([-4 * THICKNESS, 0 , 0])
      cube([2 * THICKNESS, SIDE_SUPPORT_WIDTH, BOLT_HEAD_DIAMETER]);

      translate([DEVICE_WIDTH, 0, 0])
      cube([2 * THICKNESS, SIDE_SUPPORT_WIDTH, BOLT_HEAD_DIAMETER]);

      translate([-4 * THICKNESS, 0, BOLT_HEAD_DIAMETER])
      cube([DEVICE_WIDTH + (6 * THICKNESS), SIDE_SUPPORT_WIDTH, 3 * THICKNESS]);
    }

    translate([DEVICE_WIDTH / 2, SIDE_SUPPORT_WIDTH / 2, BOLT_HEAD_RADIUS])
    rotate([0, 90, 0])
    cylinder(h = DEVICE_WIDTH * 1.5, d = BOLT_DIAMETER, center = true, $fn = 18);
  }
}

// A handy module for previewing the whole tool.
module draw_preview() {
  draw_bottom_shell();

  color("red")
  translate([0, 0, BLOCK_HEIGHT])
  draw_presser();

  color("green")
  translate([THICKNESS, ((DEVICE_LENGTH - SIDE_SUPPORT_WIDTH) / 2), (2 * BLOCK_HEIGHT) + SEED_HOLE_HEIGHT])
  top_bracket();
}

// Comment this out prior to rendering the individual parts below.
draw_preview();

// Uncomment each piece you need to print, render, and export as STL.
// The rotations are needed so this can be printed without supports.

// draw_bottom_shell();

// rotate([0, 180, 0])
// draw_presser();

// rotate([0, 180, 0])
// top_bracket();
