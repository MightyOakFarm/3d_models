# Soil Blocker

Soil blockers are simple tools that can help remove some single(ish)-use from the food system. Many people who advocate for soil blocks also claim to get better quality seed starts. The issue with soil blocker tools is they're expensive. You often want various sizes so you can start seeds in smaller blocks, then graduate those seedlings up to larger soil blocks. Being able to 3D print a simple soil blocker should save the avergage gardener quite a bit of money.

## Requirements
To use this file, you'll need two nuts and bolts, preferable at least 3/4 inch long bolts to hold this together. You'll also need some calipers to measure the bolt.

Beyond that, have an idea of what measurements you want your blocks to be.

## How to use
The .scad file has plenty of comments to walk you through the configuration of your soil blocker. You should be able to change the variables according to the comments and be on your way.

Near the bottom of the file you'll need to uncomment/comment out some lines to get it to render what you want to print. Again, there are comments there to help guide you.